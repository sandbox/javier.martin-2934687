<?php

namespace Drupal\equipos_services\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\equipos_services\HelloWorldInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HelloWorld extends FormBase {
  protected $service;
  
  public function __construct(HelloWorldInterface $service) {
    $this->service = $service;
  }
  
  public static function create (ContainerInterface $container) {
    return new static (
      $container->get('equipos_services.helloworldservice'));
  }
  
  public function getFormId() {
    return 'equipos_services_helloworld_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['textfield'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test Text'),
      '#description' => $this->t('Text loaded from equipos_services.helloworld service'),
      '#default_value' => $this->service->helloWorld(),
      '#disabled' => TRUE,
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { }
}