<?php

namespace Drupal\equipos_services;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HelloWorldService implements HelloWorldInterface {
  use StringTranslationTrait;
  
  protected $logger;
  protected $current_user;
  
  public function __construct(LoggerChannelFactoryInterface $logger, AccountProxy $current_user) {
    $this->logger = $logger->get('equipos_services - HelloWorldService');
    $this->current_user = $current_user;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('logger.factory'),
      $container->get('current_user'));
  }
  
  public function helloWorld() {
    $this->logger->info($this->t('[@current_user] HelloWorldService called.',[
      '@current_user' => $this->current_user->id()]));
    
    return $this->t('Hello world from HelloWorldService');
  }
}